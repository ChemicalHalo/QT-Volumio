# QT Volumio

This is my amateurish attempt at creating a small, relatively full featured controller for Volumio.

### Launch Options:

    _This will likely not be terribly useful in the future, but here it is._

 - ```--debug``` or ```-d``` : Adds more console messages. Probably not terribly useful.
 - ```--help``` or ```-h``` : Shows this screen. Dunno how you got here not knowing that.
 - ```--location=[location]``` or ```-l=['location']``` : Chooses the location of the Volumio install. Include the http://
 - ```--autotimer``` or ```-t``` : Activates the brute-force mode on the GUI. Currently only works on the main window. rough.
 - ```--autotimer=[time in milliseconds]``` or ```-t=[time in milliseconds]``` : Same as above, just with more user control.
 - ```--no-frame``` or ```-f``` : Makes the window a floating, borderless one. Less useful on some operating systems.
 - ```--show-albumart``` or ```-a``` : Shows the albumart. WARNING! It's slow.
 - ```--min``` or ```-m``` : Start the app minimized to tray

### Relevant Links

 - [repo](https://gitlab.com/ChemicalHalo/QT-Volumio)
 - [Volumio](https://volumio.org/)