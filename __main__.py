# QT volumio control


# Imports
import sys, os, signal, webbrowser, requests, json, time, mdv, ast, logging
# from threading import Thread
from PIL import Image
from PyQt5.QtCore import QProcess, pyqtSignal, pyqtSlot, QObject, QThread, QRect, Qt, QCoreApplication, QMetaObject, QTimer, QSize, QThreadPool, QRunnable
from PyQt5.QtWidgets import QGraphicsPixmapItem, QGraphicsScene, QApplication, QLabel, QMenu, QSystemTrayIcon, QAction, QMainWindow, QWidget, QVBoxLayout, QTabWidget, QTextEdit, QPushButton, QProgressBar, QSlider, QMenuBar, QStatusBar, QGraphicsView, QLineEdit, QDialog, QFormLayout, QCheckBox, QSpinBox, QSizePolicy, QSpacerItem, QHBoxLayout
from PyQt5.QtGui import QIcon, QPixmap, QCursor, QImage, QFont

from console import fg, bg, fx

from volumio_socket import socketIO, VolumioObject, Communicate


logging.basicConfig(format="%(message)s", level=logging.INFO)


# Globals / Defaults
TIMEOUT = 3
IDLE_TIMEOUT = None
LOCATION_URL = "volumio.local" # Notice - no http://
LOCATION_PORT = 6600
LOCAL_ROOT = os.path.abspath(os.path.dirname(__file__))
IMG_SIZE = 250,250


# app settings
TIMEOUT = 2
ICON = "logo_smallest.png"
ALBUM_DEF_ART = "logo_smaller.png"
SONG_SEP = " - "
SLEEP_TIME = 0.2
WEB_SLEEP_TIME = 1.1
AUTOTIMER = False
TIMER_INTERVAL=10000
README_LOC = "README.md"
WINDOW_TITLE= "QT Applet"
PROGRESSBAR_INTERVAL=100
WINDOW_FRAME = True
HEADER = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' }
SHOW_ALBUMART = False
MIN_TRAY = False

# API Calls. (This is mostly legacy and for bugfixes/workarounds)

API = '/api/v1/'
GET_STATE = "getState"
GET_QUEUE = "getQueue"

CMD_URL = "commands/?cmd="
PLAY_PAUSE = "toggle"
STOP_ALL = "stop"
PREVIOUS = "prev"
NEXT = "next"
RANDOM = "random"
REPEAT = "repeat"
VOLUME = "volume&volume=" # percentage volume

SETTINGS_FILE = "settings.json"

DEBUG=False

mdv.term_columns = 80

DEFAULT_DICT = {
    "settings":{
            "Location": [("volumio.local", 6600)],
            "Title": "QT Volumio",
            "Readme": "README.md",
            "Autotimer": "False",
            "Timer_interval": 6000,
            "Frameless": True,
    },
    'settings_override':False,
    'settings_location':"settings.json"
}

# Settings

SETTINGS = {
    "Location": [(LOCATION_URL, LOCATION_PORT)],#just for now
    "Icon": ICON,
    "Readme":README_LOC,
    "Albumart":ALBUM_DEF_ART,
    "Timer_interval":TIMER_INTERVAL,
    "Autotimer":AUTOTIMER,
    "Title":WINDOW_TITLE,
    "Frame":WINDOW_FRAME,
    "Minimized":MIN_TRAY,
}

def setSetting(key, value):
    glob = globals()
    glob['SETTINGS'][key] = value

def setSettings(dict):
    glob = globals()
    for k, v in dict:
        glob['SETTINGS'][k] = v
    
    # print(glob['SETTINGS'])

def setSettings(listin):
    glob = globals()
    for k in listin:
        glob['SETTINGS'][k] = listin[k]

def getSettings(key = None):
    glob = globals()
    
    if key == None:
        return glob['SETTINGS']
    else:
        try:
            return glob['SETTINGS'][key]
        except:
            return None

def getLocationUrl(n=0):
    return getSettings('Location')[n][0]

def initSettings(filename=SETTINGS_FILE):
    data = {}

    if not os.path.exists(filename):
        print("SETTINGS FILE DOES NOT EXIST!")
        if filename == "settings.json":
            with open("settings.json", 'w') as settings_file:
                settings_file.write(json.dumps(DEFAULT_DICT))

    with open(filename) as json_file:
        data_pre =json.loads(json_file.read())
        data = data_pre["settings"]

        if 'settings_override' in data_pre:
            if data_pre['settings_override']:
                data = initSettings(data_pre['settings_location'])
    
    return data

def loadSettings(filename=SETTINGS_FILE):
    print("Loading Settings from "+str(filename))

    data = initSettings(filename)
    # print(type(data))

    setSettings(data)

    # print(getSettings())

def settingsCreate(settings= None, window=None, filename = SETTINGS_FILE):
    global AUTOTIMER, TIMER_INTERVAL, ICON, DEBUG, README_LOC, ALBUM_DEF_ART, WINDOW_TITLE, SHOW_ALBUMART, WINDOW_FRAME, MIN_TRAY
    glob = globals()

    if not os.path.exists(filename):
        print("SETTINGS FILE DOES NOT EXIST!")
        if filename == "settings.json":
            with open("settings.json", 'w') as settings_file:
                settings_file.write(json.dumps(DEFAULT_DICT))


    if not settings or settings==None:
        if DEBUG: print("No settings available.")
        return
    
    for k, v in settings.items():

        if k == "Autotimer":
            AUTOTIMER = v
        elif k == "Timer_interval":
            TIMER_INTERVAL = v
        elif k == "Location":
            print(v)
            LOCATION_URL = v[0][0]
            LOCATION_PORT = v[0][1]
        elif k == "Title":
            WINDOW_TITLE = v
        elif k == "Frame":
            WINDOW_FRAME = v
        else:
            if DEBUG: print("Key not supported")


    with open(filename) as json_file:
        data_pre =json.loads(json_file.read())


    print(settings)

    for k, v in data_pre['settings'].items():

        # print(settings[k])
        if settings[k]:
            if type(data_pre['settings'][k])== list:
                if k == 'Location':
                    data_pre['settings'][k].insert(0,(settings[k][0][0], settings[k][0][1]))
                elif settings[k] == list:
                    for i in settings[k]:
                        if i not in data_pre[k]:
                            data_pre['settings'][k].insert(0,i)

                else:
                    data_pre['settings'][k].insert(0,settings[k])
            else:
                data_pre['settings'][k] = settings[k]

    with open(filename, 'w') as out_file:
        json.dump(data_pre, out_file)

# Utilities
def testwebsite(location):
    try:
        request = requests.get(location)
        if request.status_code == 200:
            return True
        else:
            return False
    except Exception as e:
        print(e)

def download_file(url): # Thanks stack for making this better than mine.
    local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk: 
                f.write(chunk)
    return local_filename

def readTxtFile(filepath, is_markdown=False):
    if os.path.exists(filepath):
        if DEBUG:print("Loading "+str(filepath))
    else:
        if DEBUG:print(filepath + "Does not exist.")
        return False
    file = open(filepath)
    content = file.read()
    file.close()
    
    return content

def timeFromSeconds(time):
    time = int(time)
    if time > 3800:
        print("THAT's in hours.")
    time_1=int(time/60)
    time_2=time%60
    time_str=str(time_1)+":"
    time_str= time_str + str(time_2) if time_2>9 else time_str+"0"+str(time_2)
    return({
        'minutes':time_1,
        'seconds':time_2,
        'string':time_str
    })

# UI/UX
def menuText(visible=False, tray=None):
    if visible:
        if tray!=None:
            tray.actionhide.setText("Hide Volumio Controller")
        return False
    else:
        if tray!=None:
            tray.actionhide.setText("Show Volumio Controller")
        return False

def launchWebsite(url = getSettings("Location")):
    print("Launching "+url)
    webbrowser.open(url)

class Pass_Object(QObject):
    string = "Hello"
    def init(self, string=None):
        self.string = string
    
    def __str__(self):
        return str(self.string)

class AlbumArtFetch(QRunnable):
    art = pyqtSignal(QObject)
    progress = pyqtSignal(int)
    
    def __init__(self, window):
        logging.info("AlbumArt Created")
        super().__init__()

    def run(self):
        logging.info("AlbumArt Running")
        out = Pass_Object()
        outs = self.setup_albumart()
        out.string=outs
        logging.info(f"OUT: {outs}")
        self.finished.emit(outs)

    def setup_albumart(self):
        if SHOW_ALBUMART == True:
            api_status = self.window.volumio.socket.status

            if api_status["albumart"].startswith("http://") or api_status["albumart"].startswith("https://")or api_status["albumart"].startswith("www."):
                img_url=api_status["albumart"]
            else:
                img_url = "http://"+getSettings("Location")[0][0]+api_status['albumart']
            
            try:
                filename = download_file(img_url)
            except:
                return "logo_256.png"
               
            else:
                # Pillow Work
                image = Image.open(filename)
                image.thumbnail(IMG_SIZE, Image.ANTIALIAS)
                image.save("albumart.png")
                os.remove(filename)

                return "albumart.png"
        
        else:
            return "logo_256.png"

def DL(img_url):
    try:
        filename = download_file(img_url)
    except Exception as e:
        logging.error(f"FUCK! {e}")
    else:
        # Pillow Work
        image = Image.open(filename)
        image.thumbnail(IMG_SIZE, Image.ANTIALIAS)
        image.save("albumart.png")
        os.remove(filename)

class DownloadArt(QRunnable):
    finished = pyqtSignal()

    def __init__(self, img_url, window):
        QObject.__init__(self)
        logging.info("Starting Download")
        self.img_url = img_url
        self.window = window

    @pyqtSlot()
    def run(self):
        try:
            out = self.setup_albumart()
            logging.info(f"Art: {out}")
            pix = QPixmap(out)
            item = QGraphicsPixmapItem(pix)
            scene= QGraphicsScene(self.window)
            scene.addItem(item)
            self.window.graphicsView.setScene(scene)
        finally:
            return


    def setup_albumart(self):
        if SHOW_ALBUMART == True:
            api_status = self.window.volumio.socket.status

            if api_status["albumart"].startswith("http://") or api_status["albumart"].startswith("https://")or api_status["albumart"].startswith("www."):
                img_url=api_status["albumart"]
            else:
                img_url = "http://"+getSettings("Location")[0][0]+api_status['albumart']
            
            try:
                filename = download_file(img_url)
            except:
                return "logo_256.png"
               
            else:
                # Pillow Work
                image = Image.open(filename)
                image.thumbnail(IMG_SIZE, Image.ANTIALIAS)
                image.save("albumart.png")
                os.remove(filename)

                return "albumart.png"
        
        else:
            return "logo_256.png"

class Updater(QRunnable):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def __init__(self, window):
        super().__init__()
        logging.info("Creating Updater")
        self.window=window

    @pyqtSlot()
    def run(self):
        try:
            logging.info("Activating Updater")
            run = True
            while run:
                time.sleep(int(TIMER_INTERVAL)/1000)
                self.window.volumio.setStatus()
            # self.window.songLoc = self.window.volumio.status['seek']
            # self.finished.emit()
        except Exception as e:
            logging.error(f"ERROR! {e}")
        finally:
            return

class MainWindow(QMainWindow):
    tray = None
    volumio = None
    settings = None
    visible = True
    songLoc = 0
    songTitle=""
    

    def __init__(self, volumio, tray=None, visible=visible):
        super(MainWindow, self).__init__()
        self.visible=visible
        print("Starting Window Construction")
        if getSettings('Frame') == False:
            self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.Tool)

        self.volumio = volumio
        self.setupUI(self)

        self.show()

        self.c = Communicate()
        self.c.visibility.connect(menuText)
        self.c.visibility.emit(True, None)
        # self.c.status_update.connect(self.update_page)

        self.progressTimer = QTimer()
        self.progressTimer.setInterval(PROGRESSBAR_INTERVAL)
        if self.volumio.socket.status != None:
            self.progressTimer.timeout.connect(self.update_progress) #convert to qthreadpool
        self.progressTimer.start()

        self.task_update()

    def task_update(self):
        print("runTasks")
        pool=QThreadPool.globalInstance()
        updater = Updater(self)
        pool.start(updater)

    def q_checkTime(self): # this needs a long-run thread.
        
        print("CHECK THE TIME!")
        self.volumio.setStatus()
        self.songLoc = self.volumio.status["seek"]

    def update_progress(self, addNumber=PROGRESSBAR_INTERVAL, status = None, reset = False):
        if status == None:
            status = self.volumio.socket.status
        
        if self.songTitle != self.volumio.socket.status['title']:
            self.songTitle = self.volumio.socket.status['title']
            self.songLoc = 0

        if self.songLoc==0:
            self.songLoc = status['seek']

        if status['status']=="play":
            songLen = status['duration']*1000
        
        
            if self.songLoc+addNumber>=songLen-addNumber+SLEEP_TIME:
                print("Time for new Progress")
                self.songProgress.setValue(0)
                
                time.sleep(SLEEP_TIME)
                self.volumio.update()
                return
                
            
            songLen = status['duration']*1000
            self.songLoc = self.songLoc+addNumber
            
            
            self.songProgress.setProperty("value", (self.songLoc/songLen)*100)
            if self.tray != None:
                self.tray.drawBar(status = status)

    def setupUI(self, MainWindow):
        if DEBUG: logging.info("\tSetting up UI")
        MainWindow.setObjectName(getSettings('Title'))
        MainWindow.resize(591, 334)
        self.setFixedSize(591, 334)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget_3 = QWidget(self.centralwidget)
        self.horizontalLayoutWidget_3.setGeometry(QRect(10, 10, 571, 271))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")
        self.horizontalLayout_3 = QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.graphicsView = QGraphicsView(self.horizontalLayoutWidget_3)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.graphicsView.sizePolicy().hasHeightForWidth())
        self.graphicsView.setSizePolicy(sizePolicy)
        self.graphicsView.setMaximumSize(QSize(260, 260))
        self.graphicsView.setObjectName("graphicsView")
        self.graphicsView.setStyleSheet("background:transparent;border:none;")
        self.horizontalLayout_3.addWidget(self.graphicsView)
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem)
        self.SongName = QLabel(self.horizontalLayoutWidget_3)
        self.SongName.setObjectName("SongName")
        self.SongName.setFixedWidth(265)
        songFont = QFont('Arial',24)
        songFont.setBold(True)
        self.SongName.setFont(songFont)
        self.verticalLayout.addWidget(self.SongName)
        self.AlbumTitle = QLabel(self.horizontalLayoutWidget_3)
        self.AlbumTitle.setObjectName("AlbumTitle")
        self.AlbumTitle.setFont(QFont('Arial',18))
        self.verticalLayout.addWidget(self.AlbumTitle)
        self.Artist = QLabel(self.horizontalLayoutWidget_3)
        self.Artist.setObjectName("Artist")
        self.verticalLayout.addWidget(self.Artist)
        spacerItem1 = QSpacerItem(20, 5, QSizePolicy.Minimum, QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem1)
        self.songProgress = QProgressBar(self.horizontalLayoutWidget_3)
        self.songProgress.setProperty("value", (self.volumio.socket.status['seek']/(self.volumio.socket.status['duration']*1000))*100)
        self.songProgress.setObjectName("songProgress")
        self.verticalLayout.addWidget(self.songProgress)
        spacerItem2 = QSpacerItem(20, 5, QSizePolicy.Minimum, QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem4 = QSpacerItem(10, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.prev = QPushButton(self.horizontalLayoutWidget_3)
        self.prev.setObjectName("prev")
        self.prev.adjustSize()
        self.horizontalLayout.addWidget(self.prev)
        self.play = QPushButton(self.horizontalLayoutWidget_3)
        self.play.setObjectName("play")
        self.play.adjustSize()
        self.horizontalLayout.addWidget(self.play)
        self.next = QPushButton(self.horizontalLayoutWidget_3)
        self.next.setObjectName("next")
        self.horizontalLayout.addWidget(self.next)
        self.horizontalLayout.addItem(spacerItem4)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.repeatCheck = QCheckBox(self.horizontalLayoutWidget_3)
        self.repeatCheck.setObjectName("repeatCheck")
        self.horizontalLayout_2.addWidget(self.repeatCheck)
        self.repeatCheck.setTristate(True)
        self.randomCheck = QCheckBox(self.horizontalLayoutWidget_3)
        self.randomCheck.setObjectName("randomCheck")
        self.horizontalLayout_2.addWidget(self.randomCheck)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QVBoxLayout() # Mute button sometimes makes the QVBox too big
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QLabel(self.horizontalLayoutWidget_3)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.volumeBar = QSlider(self.horizontalLayoutWidget_3)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.volumeBar.sizePolicy().hasHeightForWidth())
        self.volumeBar.setSizePolicy(sizePolicy)
        self.volumeBar.setOrientation(Qt.Vertical)
        self.volumeBar.setObjectName("volumeBar")
        self.volumeButton=QPushButton()
        self.volumeButton.setObjectName("mute")
        self.verticalLayout_2.addWidget(self.volumeBar)
        self.verticalLayout_2.addWidget(self.volumeButton, alignment=Qt.AlignCenter)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setGeometry(QRect(0, 0, 583, 22))
        self.menubar.setObjectName("menubar")
        self.menuSettings = QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")
        MainWindow.setMenuBar(self.menubar)
        self.serviceLabel = QLabel()
        self.serviceLabel.setObjectName("ServiceLabel")
        self.serviceLabel.setEnabled(False)
        self.statusbar = QStatusBar(MainWindow)
        # self.statusbar.setCursor(QCursor(Qt.WhatsThisCursor))
        self.statusbar.setObjectName("statusbar")
        self.statusbar.addPermanentWidget(self.serviceLabel)
        MainWindow.setStatusBar(self.statusbar)
        self.actionHide_Window = QAction(MainWindow)
        self.actionHide_Window.setObjectName("actionHide_Window")
        self.actionSettings = QAction(MainWindow)
        self.actionSettings.setObjectName("actionSettings")
        self.actionSettings.setEnabled(False)
        self.actionQuit = QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.actionWebpage = QAction(MainWindow)
        self.actionWebpage.setObjectName("actionWebpage")
        self.menuSettings.addAction(self.actionHide_Window)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionWebpage)
        self.menuSettings.addAction(self.actionSettings)
        if DEBUG:
            self.actionRefresh = QAction("Refresh")
            self.menuSettings.addAction(self.actionRefresh)
            self.actionRefresh.triggered.connect(self.q_checkTime)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionQuit)
        self.menubar.addAction(self.menuSettings.menuAction())

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

        self.pix=QPixmap("logo_256.png")
        self.item = QGraphicsPixmapItem(self.pix)
        self.scene= QGraphicsScene(self)
        self.scene.addItem(self.item)
        self.graphicsView.setScene(self.scene)

        self.window_vals(self.volumio.socket.status)
        self.setupFunctionality()

    def retranslateUi(self, MainWindow):
        if DEBUG: logging.info("\tRetranslating")
        _translate = QCoreApplication.translate
        # MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.SongName.setText(_translate("MainWindow", "Song Title"))
        self.AlbumTitle.setText(_translate("MainWindow", "Album Title"))
        self.Artist.setText(_translate("MainWindow", "Artist"))
        self.prev.setText(_translate("MainWindow", "<<"))
        self.play.setText(_translate("MainWindow", "⏯"))
        self.next.setText(_translate("MainWindow", ">>"))
        self.repeatCheck.setText(_translate("MainWindow", "Repeat"))
        self.randomCheck.setText(_translate("MainWindow", "Random"))
        self.label.setText(_translate("MainWindow", "Vol"))
        self.volumeBar.setToolTip(_translate("MainWindow", "<html><head/><body><p>Volume</p></body></html>"))
        self.menuSettings.setTitle(_translate("MainWindow", "System"))
        self.actionHide_Window.setText(_translate("MainWindow", "Hide Window"))
        self.actionSettings.setText(_translate("MainWindow", "Settings"))
        self.actionQuit.setText(_translate("MainWindow", "Quit"))
        self.actionWebpage.setText(_translate("MainWindow", "Webpage"))
 
    def task_albumart(self):
        api_status = self.volumio.socket.status

        if api_status["albumart"].startswith("http://") or api_status["albumart"].startswith("https://")or api_status["albumart"].startswith("www."):
            img_url=api_status["albumart"]
        else:
            img_url = "http://"+getSettings("Location")[0][0]+api_status['albumart']

        pool=QThreadPool.globalInstance()
        albumart = DownloadArt(img_url, self)
        pool.start(albumart)

    def setupFunctionality(self):
        
        # Menubar
        self.actionHide_Window.triggered.connect(self.toggleWindow)
        #---
        # self.menuSettings.triggered.connect()
        self.actionWebpage.triggered.connect(lambda: launchWebsite(getLocationUrl))
        # ---
        self.actionQuit.triggered.connect(sys.exit)

        # Player Controls
        self.play.clicked.connect(self.volumio.toggle)
        self.next.clicked.connect(self.volumio.next)
        self.prev.clicked.connect(self.volumio.prev)
        
        self.randomCheck.clicked.connect(lambda: self.volumio.random(self.randomCheck.isChecked()))
        self.repeatCheck.clicked.connect(lambda: self.volumio.repeat(int(self.repeatCheck.checkState())))

        self.volumeButton.clicked.connect(self.volumio.mute)
    
    def repeatState(self, status):
        if status['repeat'] == False:
            return 0
        elif status['repeat'] == True and status['repeatSingle'] == True:
            return 1
        else:
            return 2

    def update_page(self, status=None):
        self.statusbar.showMessage("Checking Volumio")
        self.statusbar.setEnabled(False)
        if status == None:
            status = self.volumio.socket.status

        if DEBUG: logging.info("update page")
        self.serviceLabel.setText(status['service']+" @ "+getSettings('Location')[0][0])
        self.repeatState(status)
        self.randomCheck.setChecked(False if status['random']== None else status['random'])
        self.repeatCheck.setCheckState(self.repeatState(status))
        if self.repeatState(status) == 1:
            self.repeatCheck.setText("Repeat Song")
        else:
            self.repeatCheck.setText("Repeat")

        if status['title'] != self.SongName.text():
            if DEBUG: logging.info("Update Status")
            self.songInfo(status)
            self.task_albumart()
        elif status['title'] == self.SongName.text() and (status['repeat'] == True and status['repeatSingle'] == True):
            self.songInfo(status)
            self.songLoc = 0
            

        self.setPlayPauseVals(status)

        # tray update
        if DEBUG: logging.info("update tray")
        if self.tray != None:
            self.tray.setMenuOptions(status=status)

        if DEBUG: logging.info("update progressbar")
        self.update_progress(PROGRESSBAR_INTERVAL)

    def window_vals(self, status):
        if DEBUG: logging.info("window_vals")
        self.serviceLabel.setText(status['service']+" @ "+getSettings('Location')[0][0])
        self.randomCheck.setChecked(False if status['random']== None else status['random'])
        self.repeatCheck.setCheckState(self.repeatState(status))
        self.songInfo(status)
        self.task_albumart()
        self.setPlayPauseVals(status)
        self.prev.setText("⏮︎")
        self.next.setText("⏭︎")
        self.volumeButton.setText("🔇")
        self.volumeButton.setFixedWidth(30)
        self.prev.setFixedWidth(30)
        self.next.setFixedWidth(30)
        self.play.setFixedWidth(30)
        
    def updateAlbumArt(self, status): # Here for posterity.
        # if SHOW_ALBUMART == True or getSettings("Show_Albumart") == True:
        #         self.longAlbum() #Put a setting on this. it takes so goddamn long.
        # else:
        #     if not status:
        #         status=self.volumio.socket.status
        #     # print(status)
        #     image_url ="http://"+getSettings('Location')[0][0]+status['albumart'] # this section works, but is only marginally faster
        #     image_data = requests.get(image_url)

        #     if image_data.status_code == 200:
        #         image = QImage()
        #         image.loadFromData(image_data.content)
        #         pix = QPixmap(image)
        #     else:
        #         pix=QPixmap("logo_256.png")

        #     pix=QPixmap("logo_256.png")                
        #     item = QGraphicsPixmapItem(self.pix)
        #     scene= QGraphicsScene(self)
        #     scene.addItem(item)
        #     self.graphicsView.setScene(scene)
        pass

    def songInfo(self, status):
        self.SongName.setText(status['title'])
        self.AlbumTitle.setText(status['album'])
        self.Artist.setText(status['artist'])
        self.volumeBar.setValue(status['volume'])
        # self.randomCheck.setChecked(bool(status['random']) if status['random'] != None else False )
        # self.repeatCheck.setChecked(status['repeat'] if status['repeat'] != None else False )
        # self.repeatCheck.setEnabled(False) # Not working yet.

    def setPlayPauseVals(self, status):
        if status['status'] == "play":
            self.play.setText("⏸️")
        elif status['status'] == "pause":
            self.play.setText("▶️")
        else:
            self.play.setText("⏯︎")

    def toggleWindow(self):
        if self.visible:
            self.hide()
            self.visible=False
            self.c.visibility.emit(self.visible, self.tray)
        else:
            self.show()
            self.visible=True
            self.c.visibility.emit(self.visible, self.tray)


class SysTray(QSystemTrayIcon):

    def __init__(self, main=None):
        super().__init__()

        self.main=main

        self.status = self.main.volumio.status
        self.icon = QIcon(ICON)

        self.tray = QSystemTrayIcon()

        self.tray.setIcon(self.icon)
        self.tray.setVisible(True)
        self.tray.setToolTip("Volumio Controller")

        self.c = Communicate()
        self.c.visibility.connect(menuText)
        # self.c.visibility.emit(False, self)
        self.c.status_update.connect(self.setMenuOptions)

        self.menu = QMenu()
            
        self.drawMenuOptions()
     
        self.tray.setContextMenu(self.menu)

        self.tray.show()

    def setMenuOptions(self, status=None):

        if not status or status == None:
            status = self.main.volumio.socket.status
            # print(status)

        self.error = QAction("ERROR!")
        self.error.setEnabled(False)

        if type(status)!=dict or not status:
            # print(status)
            self.menu.addAction(self.error)
            return

        if status['status'] == 'play':
            self.playstat.setText('Playing')
        elif status['status'] == 'stop':
            self.playstat.setText('Stopped')
        elif status['status'] == 'pause':
            self.playstat.setText('Paused')

        self.songName = QAction(status['title'])
        self.songAlbum = QAction(status['album'])
        self.songArtist = QAction(status['artist'])
        self.songDuration = QAction(timeFromSeconds(status['duration'])['string'])

        self.songName.setEnabled(False)
        self.songAlbum.setEnabled(False)
        self.songArtist.setEnabled(False)
        self.songDuration.setEnabled(False)

        self.menu.addAction(self.songName)
        self.menu.addAction(self.songAlbum)
        self.menu.addAction(self.songArtist)
        self.menu.addAction(self.songDuration)

    def drawBar(self, status=None):
        if status == None:
            status = self.main.volumio.socket.status         
        seek=status['seek']
        progressbar = "╠"
        width=20
        seek_perc = (seek/int(status['duration']*1000))
        seek_part=int(seek_perc * width)
        x=0
        while x < width:
            if x == seek_part:
                progressbar+="╪"
            else:
                progressbar+="═"
            
            x+=1
        progressbar+="╣"
        self.prog_bar.setText(progressbar)

    def drawMenuOptions(self, status=None):
        # This block of stuff is what should be rechecked.
        if status == None:
            status = self.status
        if type(status) == dict:
            pass
        else:
            print("error")
            self.err = QAction("No Connection to Volumio!")
            self.err.setEnabled(False)
            self.menu.addAction(self.err)

        self.playstat = QAction("Stopped")
        self.prev = QAction("Previous")
        self.next = QAction("Next")

        self.playstat.triggered.connect(self.main.volumio.toggle)

        self.menu.addAction(self.playstat)
        self.menu.addAction(self.prev)
        self.menu.addAction(self.next)

        self.setMenuOptions(self.status)

        self.prog_bar = QAction("---")
        self.prog_bar.setEnabled(False)
        self.menu.addAction(self.prog_bar)

        self.drawBar()

        if self.main!=None:
            self.menu.addSeparator()
            self.actionhide = QAction('Hide Volumio Controller')
    
            self.actionhide.triggered.connect(self.main.toggleWindow)
            self.menu.addAction(self.actionhide)

        self.web_page = QAction("Launch Webpage")

        if testwebsite("http://"+getSettings('Location')[0][0]):
            self.web_page.triggered.connect(lambda: launchWebsite(url="http://"+getSettings('Location')[0][0]))
            self.menu.addAction(self.web_page)
        
        self.menu.addSeparator()
        self.quit = QAction("Quit") 
        self.quit.triggered.connect(QApplication.quit)
        self.menu.addAction(self.quit) 



# System
def startup():
    global AUTOTIMER, TIMER_INTERVAL, ICON, DEBUG, README_LOC, ALBUM_DEF_ART, WINDOW_TITLE, SHOW_ALBUMART, WINDOW_FRAME, MIN_TRAY
    print("Starting Program")

    message = ["Starting Settings Check"]
    loadSettings()
    settings={}

    i = len(sys.argv)
    for a in sys.argv:

        if a.startswith("-c=") or a.startswith("--config="):
            l = a.split("=")[1]
            message.append("\tDifferent Config File at "+str(l))
            loadSettings(filename=l)
        else: 
            if a == "-d" or a == "--debug":
                message.append("\tDEBUG Mode Activated")
                DEBUG = True

            if a == "-a" or a =="--show-albumart":
                message.append("\tShowing Albumart. This may be slow.")
                SHOW_ALBUMART = True
                setSetting('Show_Albumart', True)

            if a == "-f" or a == "--no-frame":
                message.append("\tFrameless window.")
                WINDOW_FRAME = False
                setSetting('Frame', False)

            if a == "-m" or a == "--min":
                message.append("\tStart Minimized")
                MIN_TRAY = True
                setSetting('Minimized', True)
                
            if a.startswith("-l=") or a.startswith("--location="):
                l = a.split("=")[1]
                p = l.split(":")
                q=[]
                for j in p:
                    if j.startswith("http"):
                        continue
                    else:
                        q.append(j)

                if len(q) == 1:
                    q.append(6600)

                if len(q) <1 or len(q) >=3:
                    message.append("Incorrect Location Formatting.")

                else:
                    setSetting('Location', [(q[0], q[1])])

                    LOCATION_URL = q[0]
                    LOCATION_PORT = q[1]

                    message.append("\tNew Location: "+str(q[0])+":"+str(q[1]))

            if a == "-t" or a == "--autotimer":
                AUTOTIMER = True

                setSetting('Autotimer', True)
                message.append("\tAutotimer Activated")
            
            if a.startswith("-t=") or a.startswith("--autotimer="):
                t = a.split("=")[1]

                AUTOTIMER=True
                TIMER_INTERVAL = t

                setSetting('Autotimer', True)
                setSetting('Timer_interval', t)

                message.append("\tAutotimer @ "+str(t)+" ms")

            if a.startswith("--help==") or a.startswith("-h="):
                l = a.split("=")[1]
                try:
                    f=open(l)
                except IOError:
                    print("! Alternate file not accessible.")
                    exit()
                finally:
                    f.close()
                    print(readTxtFile(l))
                    exit()

            if a == "--help" or a == "-h":
                readme = getSettings('Readme')
                if getSettings('Readme').split(".")[1].lower() == "md":
                    formatted = mdv.main(readTxtFile(readme))
                    print(formatted)
                else:
                    print(readTxtFile(readme))
                exit()

    return {'messages':message}

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setApplicationName("Volumio Controller")
    print("Starting ",fg.green, fx.bold, "QT Volumio", fx.end, sep='')
    messages = startup()['messages']

    if DEBUG:
        for m in messages:
            print("\t",fx.italic,m,fx.end,sep='')

    volumio = VolumioObject(location=getLocationUrl(), nickname="Default")
    mainWindow = MainWindow(volumio)
    volumio.setWindow(mainWindow)
    tray = SysTray(main=mainWindow)
    mainWindow.tray=tray

    sys.exit(app.exec_())