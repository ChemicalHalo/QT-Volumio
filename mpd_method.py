import sys, os, requests

from mpd import MPDClient


# Globals
TIMEOUT = 10
IDLE_TIMEOUT = None
LOCATION_URL = "andy.hifi"
LOCATION_PORT = "6600"


# Start Client
client = MPDClient()
client.timeout = TIMEOUT
client.idletimeour = IDLE_TIMEOUT
client.connect(LOCATION_URL, LOCATION_PORT)

# Do Things
print(client.mpd_version)
print(client.currentsong())

# Close Client
client.close()
client.disconnect()


