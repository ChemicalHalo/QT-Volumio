import sys, io, socket, logging
# Socket communications
from socketIO_client import SocketIO, BaseNamespace
from PyQt5.QtCore import QProcess, pyqtSignal, QObject, QThread
from PyQt5.QtWidgets import QSystemTrayIcon, QMainWindow
from console import fg, bg, fx

logging.basicConfig(format="%(message)s", level=logging.INFO)

DEBUG = True

# Signals
class Communicate(QObject):
    visibility = pyqtSignal(bool, QSystemTrayIcon)
    status_update = pyqtSignal(dict, QMainWindow)

# Socket
class socketIO(object):
    status = {}

    def __init__(self, location = 'andy-hifi.local', port = 3000, window=None):
        self.window = window
        self.location = location
        self.port=port
        self.muted = True # need to instantiate the status of this connection
        self.c = Communicate()
        self.wrapper(self.getStatus)

    def wrapper(self, func_name):
        self.socket = SocketIO(self.location, self.port, self.on_connect())
        if DEBUG:print(fg.yellow,"Starting a wrapper:",fx.end)
        func_name()
        self.socket.wait(seconds=1)

    def on_response(self,*args):
        if DEBUG:print(fg.red,"Response: ",fx.end,args)
        if DEBUG:print(type(args))
        self.status = args[0]
        self.c.status_update.emit(self.status, self.window)
        return(args)

    def on_pushState(self,*args):
        
        self.status = args[0]
        if self.window != None:
            print("Pushstate to: "+str(self.window.volumio.nickname))
            self.c.status_update.emit(self.status, self.window)

    def on_connect(self):
        print(fg.green,'[Connected]',fx.end)

    def play(self):
        self.socket.emit('play')
        self.socket.on('pushState', self.on_pushState)
    
    def pause(self):
        self.socket.emit('pause')
        self.socket.on('pushState', self.on_pushState)
    
    def stop(self):
        self.socket.emit('stop')
        self.socket.on('pushState', self.on_pushState)
    
    def next(self):
        self.socket.emit('next')
        self.socket.on('pushState', self.on_pushState)

    def previous(self):
        self.socket.emit('prev')
        self.socket.on('pushState', self.on_pushState)

    def seek(self, n=0):
        self.socket.emit('seek','n')
        self.socket.on('pushState', self.on_pushState)

    def mute(self):
        self.socket.emit('mute','')
        self.socket.on('pushState', self.on_pushState)

    def unmute(self):
        self.socket.emit('unmute','')
        self.socket.on('pushState', self.on_pushState)

    def enable_random(self):
        logging.info(f"Attempting to enable random")
        self.socket.emit('setRandom',{"value":True})
        self.socket.on('pushState', self.on_pushState)

    def disable_random(self):
        logging.info(f"Attempting to disable random")
        self.socket.emit('setRandom',{"value":False})
        self.socket.on('pushState', self.on_pushState)

    def enable_repeat(self):
        self.socket.emit('setRepeat', {'value':True, "repeatSingle":False})
        self.socket.on('pushState', self.on_pushState)

    def enable_song_repeat(self):
        self.socket.emit('setRepeat', {'value':True, "repeatSingle":True})
        self.socket.on('pushState', self.on_pushState)
    
    def disable_repeat(self):
        self.socket.emit('setRepeat', {'value':False, "repeatSingle":False})
        self.socket.on('pushState', self.on_pushState)

    def getState(self):
        if DEBUG:print(fg.green,"\tGetting State",fx.end)
        self.socket.emit('getState','')
        self.socket.on('pushState', self.on_pushState)

    def getStatus(self):
        self.socket.emit('getState','')
        self.socket.on('pushState', self.on_pushState)
        
    def passState(self):
        self.socket.emit('getState','')
        self.socket.on('pushState', self.on_pushState)
        return self.status


    def __del__(self):
        print(fg.red,'[Disconnected]',fx.end)
    
# Volumio Entity
class VolumioObject(object):
    status = {}
    
    def __init__(self, location = 'volumio.local', port = 3000, nickname="Default"):
        self.window=None
        self.nickname = nickname
        print("Creating ",fg.orange, fx.bold,self.nickname,fx.end," at ",fx.italic,location,fx.end, sep='')
        self.socket = socketIO(location=location, port = port)
        self.socket.wrapper(self.socket.getState)
        # self.setStatus()

        self.c = Communicate()
        self.c.status_update.connect(self.minorSetStatus)

    def setWindow(self, window):
        self.window = window
        self.socket.window= window
        self.socket.c.status_update.connect(self.window.update_page)

    def setStatus(self):
        self.socket.wrapper(self.socket.getState)
        self.status = self.socket.status

    def minorSetStatus(self):
        self.status = self.socket.status

    def mute(self):
        if self.socket.status['mute'] == False:
            self.socket.wrapper(self.socket.mute)
        else:
            self.socket.wrapper(self.socket.unmute)

    def setVolume(self, vol=100):
        self.socket.emit('volume', vol) #needs a quick ping-back check
        self.status['volume'] = vol

    def getStatus(self, key=None):
        # print('Getstatus')
        if key == None:
            return self.socket.getStatus()
        else:
            if key in self.status:
                return self.status[key]
        
    def play(self, id=None):
        if id == None:
            self.socket.wrapper(self.socket.play)

    def toggle(self):
        if self.socket.status['status'] == 'play':
            self.socket.wrapper(self.socket.pause)
        else:
            self.socket.wrapper(self.socket.play)
        
    def pause(self):
        self.socket.wrapper(self.socket.pause)

    def next(self):
        self.socket.wrapper(self.socket.next)

    def prev(self):
        self.socket.wrapper(self.socket.previous)

    def random(self, val=None):
        print("RANDOM!")
        if type(val) == bool:
            if val == True:
                self.socket.wrapper(self.socket.enable_random)
                self.minorSetStatus()
                print("ENABLE_RANDOM")
            else:
                self.socket.wrapper(self.socket.disable_random)
                print("DISABLE_RANDOM")
                self.minorSetStatus()
        else:
            raise TypeError("Random cannot accept the valuetype: "+str(type(val)))

    def repeat(self, val):
        print("REPEAT!")
        if type(val) == int:
            if val == 2:
                self.socket.wrapper(self.socket.enable_repeat)
            elif val == 1:
                self.socket.wrapper(self.socket.enable_song_repeat)
            else:
                self.socket.wrapper(self.socket.disable_repeat)
        else:
            raise TypeError("Repeat cannot accept the valuetype: "+str(type(val)))
    
    def update(self):
        self.socket.wrapper(self.socket.getStatus)

    def __str__(self):
        return("self.nickname")

    def state(self):
        out="Status:\n"
        for k, v in self.status.items():
            out += "\t"+k+": "+str(v)+"\n"
        return out
